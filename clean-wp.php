<?php
/*
Plugin Name: 	Clean WP
Plugin URL:		https://bitbucket.org/solutionmachine/clean-wp
Description:	A simple mu-plugin to clean up misc annoyances in WP
Version:		1.0.0
Author:			Chad Porter
Author URI:		https://www.solutionmachine.com

License:		MIT License
License URI:	https://opensource.org/licenses/MIT
*/

namespace SolutionMachine\WPUtils;

// Exit if the plugin is accessed directly
if (!defined('ABSPATH')) exit;

// Wrap in a class exists check so we don't somehow fire this up twice
if (!class_exists('CleanWP')) :

/**
 * This class contains the core CleanWP functionality
 */
class CleanWP
{	
	private function __construct()
	{
		
	}
	
	public static function init()
	{
		self::cleanDashboard();
		self::removeWPAutoP();
		self::useRelativeLinks();
		self::stopJPEGDegradation();
	}
	
	/**
	 * Various actions that clean up the WP admin panel dashboard
	 */
	private static function cleanDashboard()
	{
		\add_filter('admin_footer_text', [__CLASS__, 'changeAdminFooterText']);
		\add_action('admin_head', [__CLASS__, 'hideHelpTab']);
		
		\add_filter('pre_site_transient_update_core', [__CLASS__, 'removeUpdateNags']);
		\add_filter('pre_site_transient_update_plugins', [__CLASS__, 'removeUpdateNags']);
		\add_filter('pre_site_transient_update_themes', [__CLASS__, 'removeUpdateNags']);
	}
	
	/**
	 * Replace the footer text in the admin menu to get rid of the WordPress link
	 */
	public static function changeAdminFooterText()
	{
		echo "";		
	}
	
	/**
	 * Hides the help tab on the admin panel
	 */
	public static function hideHelpTab()
	{
		$screen = get_current_screen();
		
		$screen->remove_help_tabs();
	}
	
	/**
	 * Stops WP from bothering you about updates
	 */
	public static function removeUpdateNags()
	{
		global $wpVersion;
		
		return [
			'last_checked' => time(),
			'version_checked' => $wpVersion
		];
	}
	
	/**
	 * Remove the abomination that is wpautop
	 */
	private static function removeWPAutoP()
	{
		\remove_filter('the_content', 'wpautop');
		\remove_filter('the_excerpt', 'wpautop');
		
		\add_action('acf/init', [__CLASS__, 'removeACFWPAutoP'], 15);
	}
	
	/**
	 * Removes wpautop for acf content fields
	 */
	public static function removeACFWPAutoP()
	{		
		\remove_filter('acf_the_content', 'wpautop');
	}
	
	/**
	 * Make WP use relative internal links
	 */
	private static function useRelativeLinks()
	{
		\add_filter('post_link', [__CLASS__, 'makeInternalLinkRelative'], 10, 2);
		\add_filter('post_type_link', [__CLASS__, 'makeInternalLinkRelative'], 10, 2);
		\add_filter('page_link', [__CLASS__, 'makeInternalLinkRelative'], 10, 2);
	}
	
	/**
	 * Makes any internal site links (links to the same domain the site is on)
	 * relative links. This will make it so dev, staging, and prod don't compete
	 * for link URLs all the time.
	 */
	public static function makeInternalLinkRelative($url, $post)
	{
		$urlParts = parse_url($url);
		$homeUrlParts = parse_url(home_url());
		
		if (array_key_exists('host', $urlParts) && $urlParts['host'] == $homeUrlParts['host']) {
			$url = \wp_make_link_relative($url);
		}
		
		return $url;
	}
	
	/**
	 * Set JPEG quality to 100 so repeated saves don't degrate the image
	 */
	private static function stopJPEGDegradation()
	{
		\add_filter('jpeg_quality', function () { return 100; });
	}
}

add_action('plugins_loaded', ['SolutionMachine\WPUtils\CleanWP', 'init']);

endif;